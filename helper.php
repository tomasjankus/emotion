<?php

Class Helper {
	private static $updated = [];

	public static function addUpdatedField($field, $value) {
		self::$updated[$field] = $value;
	}

	public static function getPostField($field) {
		if (isset(self::$updated[$field])) {
			return self::$updated[$field];
		}

		if (Validator::hasErrors() && isset($_POST[$field])) {
			return trim(strip_tags($_POST[$field]));
		}

		return '';
	}

	public static function getOffset($page = false) {
		if ($page === false) {
			return self::getCurrentOffset();
		}
		return $page * Config::$messagesPerPage;
	}

	public static function getCurrentOffset() {
		if (empty($_GET['page']) || (intval($_GET['page']) < 0)) {
			return 0;
		}
		return intval($_GET['page']) * Config::$messagesPerPage;
	}

	public static function getPageNumber($mod = 0) {
		if (empty($_GET['page'])) {
			return $mod > 0 ? $mod : 0; // no negative pages
		}
		return (intval($_GET['page']) + $mod) > 0 ? (intval($_GET['page']) + $mod) : 0;  // no negative pages
	}

	public static function getPrevPage() {
		return self::getPageNumber(-1);
	}

	public static function getCurrentPage() {
		return self::getPageNumber();
	}

	public static function getNextPage() {
		return self::getPageNumber(1);
	}

	public static function getPostData() {
		if (empty($_POST)) {
			return false;
		}

		return [
				'fullname' => trim(strip_tags($_POST['fullname'])),
				'birthdate' => trim(strip_tags($_POST['birthdate'])),
				'message' => trim(strip_tags($_POST['message'])),
				'email' => trim(strip_tags($_POST['email']))
			];
	}

	public static function getHtml($template) {
		ob_start();
		include(Config::$assetRelUrl.$template);
		$result = ob_get_contents();
		ob_end_clean();
		return $result;
	}

	public static function ajaxResponse($response) {
		echo json_encode($response);
		die();
	}
}