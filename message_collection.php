<?php

require_once("message.php");

Class MessageCollection {
	private $messages = [];
	private $total;

	public function __construct() {
		$this->fetchPage();
	}

	public function fetchPage($page = false) {
		$db = MySqlDatabase::getInstance();
		$msgq = $db->prepare("SELECT fullname, birthdate, message, DATE_FORMAT(created, '%Y %m %d %H:%i'), TIMESTAMPDIFF(YEAR,birthdate,CURDATE()), email FROM messages ORDER BY id DESC LIMIT ?,?");
		$offset = Helper::getOffset($page);
		$mpp = Config::$messagesPerPage;

		$msgq->bind_param('ii', $offset, $mpp);
		$msgq->execute();

		$msgq->store_result();
		$msgq->bind_result($fullname, $birthdate, $message, $posted, $age, $email);

		$this->clear();
		while ($msgq->fetch())
		{
			$this->add($fullname, $birthdate, $message, $posted, $age, $email);
		}

		$msgq = $db->prepare("SELECT COUNT(id) FROM messages");
		$msgq->execute();
		$msgq->bind_result($count);
		$msgq->fetch();
		$this->total = $count;
	}

	public function add($fullname, $birthdate, $message, $posted, $age, $email = '') {
		$this->messages[] = new Message($fullname, $birthdate, $message, $posted, $age, $email);
	}

	public function clear() {
		unset($this->messages);
		$this->messages = [];
	}

	public function prepend($msg) {
		array_unshift($this->messages, $msg);
	}

	public function create($data) {
		$db = MySqlDatabase::getInstance();
		$msgq = $db->prepare("INSERT INTO messages (fullname, birthdate, message, email) VALUES (?, ?, ?, ?)");
		$fullname = $data['fullname'];
		$birthdate = $data['birthdate'];
		$message = $data['message'];
		$email = $data['email'];
		$msgq->bind_param('ssss', $fullname, $birthdate, $message, $email);
		$msgq->execute();
		$this->addLastInserted();
	}

	public function read($id) {
		$db = MySqlDatabase::getInstance();
		$msgq = $db->prepare("SELECT fullname, birthdate, message, DATE_FORMAT(created, '%Y %m %d %H:%i'), TIMESTAMPDIFF(YEAR,birthdate,CURDATE()), email FROM messages WHERE id=?");
		$msgq->bind_param('i', $id);
		$msgq->execute();
		$msgq->store_result();
		$msgq->bind_result($fullname, $birthdate, $message, $posted, $age, $email);
		$msgq->fetch();
		return new Message($fullname, $birthdate, $message, $posted, $age, $email);
	}

	public function addLastInserted() {
		$db = MySqlDatabase::getInstance();
		$msg = $this->read($db->insert_id);
		$this->prepend($msg);
	}

	public function getAll() {
		return $this->messages;
	}

	public function getPageCount() {
		return ceil($this->total/Config::$messagesPerPage);
	}
}