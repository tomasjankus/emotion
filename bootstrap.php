<?php

// debug mode
error_reporting(E_ALL);
ini_set('display_errors', 1);



require_once("config.php");
require_once("mysqldatabase.php");
require_once("helper.php");

require_once("validator.php");
require_once("message_collection.php");
require_once("application.php");