<?php

Class Validator {
	private static $errors = [];

	public static function addError($field, $error) {
		self::$errors[$field] = $error;
	}

	public static function addErrors(array $errors) {
		//foreach addError()
	}

	public static function hasErrors() {
		return (!empty(self::$errors));
	}

	public static function getErrorData() {
		$response = [];
		foreach (self::$errors as $field => $error) {
			$response[$field] = [];
			$response[$field]['error'] = $error;
			$response[$field]['updated'] = Helper::getPostField($field);
		};
		return $response;
	}

	public static function getFieldClass($field) {
		if (!self::hasErrors()) {
			return Config::$absentClass;
		}

		return (empty(self::$errors[$field])) ? Config::$validClass : Config::$errorClass;
	}

	public static function validate($data) {
		return self::isValid($data) ? true : self::$errors;
	}

	private static function isValid($data) {
		self::isValidFullName($data['fullname']);
		self::isValidBirthDate($data['birthdate']);
		self::isValidEmail($data['email']);
		self::isValidMessage($data['message']);

		return (empty(self::$errors));
	}

	private static function isValidFullName($fullname) {
		/*
			Need to make sure the PHP locale is set correctly for non-latin characters to pass ctype_alpha()
		*/

		if ((str_word_count($fullname) == 2) && (ctype_alpha(preg_replace('/\s+/', '', $fullname)))) {
			return true;
		}

		self::addError('fullname', 'Vardą turi sudaryti du žožiai');
		return false;
	}

	private static function isValidBirthDate($birthdate) {
		if (($timestamp = strtotime($birthdate)) !== false) {
			if ($timestamp > time()) {
				self::addError('birthdate', 'Gimimo data negali būti ateityje');
				return false;
			}
			$formattedDate = date(Config::$dateFormat, $timestamp);
			if (($birthdate) == $formattedDate) {
				// date conforms to our format
				return true;
			} else {
				// date was parsed, not sure however if the user has that exact date in mind
				// EG: 1985/10/11 = (month/day? day/month?)
				// Or 01 02 30 = March 2nd or February 1st? (no way to know what year it is)

				// Reformat to Config::$dateFormat and let the user double check it

				self::addError('birthdate', 'Ar teisingai supratome ivestą datą?');
				Helper::addUpdatedField('birthdate', $formattedDate);
				return false;
			}
		}
		self::addError('birthdate', 'Neteisinga gimimo data');
		return false;
	}

	private static function isValidEmail($email) {
		// may be empty
		if (empty($email)) {
			return true;
		}

		// PHP >= 5.2
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return true;
		}

		self::addError('email', 'Neteisingas el pašto adresas');
		return false;
	}

	private static function isValidMessage($message) {
		if (!empty($message)) {
			return true;
		}

		self::addError('message', 'Prašome įvesti žinutę');
		return false;
	}
}