<?php

Class Message {
	public $fullname;
	public $birthdate;
	public $message;
	public $email;
	public $posted;
	public $age;

	public function __construct ($fullname, $birthdate, $message, $posted, $age, $email = '') {
		$this->fullname = $fullname;
		$this->birthdate = $birthdate;
		$this->message = $message;
		$this->posted = $posted;
		$this->email = $email;
		$this->age = $age;
	}

}