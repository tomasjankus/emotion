<?php
	$currentPage = Helper::getCurrentPage();
	if ($currentPage) {
		?>
		<a href="?page=<?php echo Helper::getPrevPage(); ?>" title="atgal">atgal</a>
		<?php
	} else {
		?>atgal<?php
	}
?>

<?php
	$pageCount = Application::$messages->getPageCount();
	for ($i=1; $i<=$pageCount; $i++) {
		if ($i != $currentPage+1) {
		?>
		<a href="?page=<?php echo $i-1; ?>" title="<?php echo $i; ?>"><?php echo $i; ?></a>
		<?php
		} else {
			echo $i;
		}
	}
?>

<?php
	if ($pageCount > $currentPage+1) {
		?>
		<a href="?page=<?php echo Helper::getNextPage(); ?>" title="toliau">toliau</a>
		<?php
	} else {
		?>toliau<?php
	}
?>
