<?php
	$msg_data = Application::$messages->getAll();
	if (empty($msg_data)) {
		?>
		<li>
			<strong>Šiuo metu žinučių nėra. Būk pirmas!</strong>
		</li>
		<?php
	} else {
		foreach ($msg_data as $msg) {
			?>
			<li>
				<span><?php echo $msg->posted; ?></span> <?php
					if (!empty($msg->email)) {
						?>
						<a href="mailto:<?php echo $msg->email; ?>"><?php echo $msg->fullname; ?></a>
						<?php
					} else {
						echo $msg->fullname;
					}
				?>, <?php echo $msg->age; ?> m.<br/>
				<?php echo $msg->message; ?>
			</li>
			<?php
		}
	}
?>
