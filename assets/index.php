<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Žinutės</title>
		<link rel="stylesheet" media="screen" type="text/css" href="<?php echo Config::$assetRelUrl; ?>css/screen.css" />
		<script type="text/javascript" src="<?php echo Config::$assetRelUrl;?>js/libs/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="<?php echo Config::$assetRelUrl;?>js/common.js"></script>
	</head>
	<body>
		<div id="wrapper">
			<h1>Jūsų žinutės</h1>
			<form action="?page=0" method="post" id="message-form">
				<p class="validate fullname <?php echo Validator::getFieldClass('fullname'); ?>">
					<label for="fullname">Vardas, pavardė *</label><br/>
					<input id="fullname" type="text" name="fullname" value="<?php echo Helper::getPostField('fullname'); ?>" />
				</p>
				<p class="validate birthdate <?php echo Validator::getFieldClass('birthdate'); ?>">
					<label for="birthdate">Gimimo data *</label><br/>
					<input id="birthdate" type="text" name="birthdate" value="<?php echo Helper::getPostField('birthdate'); ?>" />
				</p>
				<p class="validate email <?php echo Validator::getFieldClass('email'); ?>">
					<label for="email">El.pašto adresas</label><br/>
					<input id="email" type="text" name="email" value="<?php echo Helper::getPostField('email'); ?>" />
				</p>
				<p class="validate message <?php echo Validator::getFieldClass('message'); ?>">
					<label for="message">Jūsų žinutė *</label><br/>
					<textarea id="message" name="message"><?php echo Helper::getPostField('message'); ?></textarea>
				</p>
				<p>
					<span>* - privalomi laukai</span>
					<input id="is-validated" type="hidden" name="is-validated" value="0" />
					<input type="submit" value="Skelbti" />
					<img id="loader" class="dn" src="<?php echo Config::$assetRelUrl;?>img/ajax-loader.gif" alt="" />
				</p>
			</form>
			<ul id="messageList">
				<?php include("message_list.php"); ?>
			</ul>
			<p id="pagination">
				<?php include("pagination.php"); ?>
			</p>
		</div>
	</body>
	<script type="text/javascript">
		<?php include("js_params.php"); // comment inside ?>
	</script>
</html>
