<?php
	/* printing some params for JS so we don't have to re-define them again in .js */
	$params = [
		'errorClass' => Config::$errorClass,
		'validClass' => Config::$validClass,
		'absentClass' => Config::$absentClass
	];
	echo 'tjParams = '.json_encode($params);
?>