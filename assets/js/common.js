var tjApplication = {
	init: function() {
		this.$msgForm = $('#message-form');
		this.$loaderImg = $('#loader');
		this.enableForm();

		var that = this;
		this.$msgForm.on('submit', function(e) {
			e.preventDefault();
			var data = that.$msgForm.serialize();
			that.disableForm();
			$.post('?ajax=1', data,	function(response) {
					that.enableForm();
					if (response['valid']) {
						that.updateHtml(response['html']);
						that.clearFormClasses();
						that.clearMessageText();
					} else {
						that.addErrors(response['errors']);
					}
				}, 'json');
		});
	},
	disableForm: function() {
		this.$msgForm.find(":input").prop("disabled", true);
		this.$loaderImg.toggleClass('dn', false);
	},
	enableForm: function() {
		this.$msgForm.find(":input").prop("disabled", false);
		this.$loaderImg.toggleClass('dn', true);
	},
	updateHtml: function(html) {
		$.each(html, function(id, value) {
			$('#' + id).html(value);
		});
	},
	addErrors: function(errors) {
		/* tjParams is set by PHP, so we don't have to define it multiple times (PHP and JS) */
		this.clearFormClasses().addClass(tjParams.validClass);
		var that = this;
		$.each(errors, function(field, error) {
			var fieldBlock = $('.' + field);
			fieldBlock.toggleClass(that.getValidErrorClasses());
			fieldBlock.children('label').text(error['error']);
			if (typeof error['updated'] != 'undefined') {
				fieldBlock.children(':input').val(error['updated']);
			}
		});
	},
	getValidErrorClasses: function() {
		return tjParams.validClass + ' ' + tjParams.errorClass;
	},
	getAllClasses: function() {
		return tjParams.validClass + ' ' + tjParams.errorClass + ' ' + tjParams.absentClass;
	},
	clearFormClasses: function() {
		return this.$msgForm.children('p.validate').removeClass(this.getAllClasses());
	},
	clearMessageText: function() {
		/*	only clearing textarea, so the user doesn't have to fill
			his name/dob/email when entering further messages */
		$('#message').val('').focus();
	},
};

$(function() {
	tjApplication.init();
})
