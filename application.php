<?php

Class Application {
	public static $messages;
	private static $_data;

	public static function init() {
		self::$messages = new MessageCollection();
		self::$_data = Helper::getPostData();

		if (self::$_data) {
			if (Validator::validate(self::$_data) === true) {
				self::$messages->create(self::$_data);
			}
			if (!empty($_GET['ajax'])) {
				self::ajaxSubmit(); //returns JSON and dies somewhere deep inside :)
			}
		}

		self::_render('index.php');
	}

	private static function _render($template) {
		require_once(Config::$assetRelUrl.$template);
	}

	public static function ajaxSubmit() {
		$response = [];
		$response['valid'] = false;
		if (Validator::hasErrors()) {
			$response['errors'] = Validator::getErrorData();
		} else {
			self::$messages->fetchPage(0);
			$response['valid'] = true;
			$response['html'] = [
				'messageList' => self::getMessageListHtml(),
				'pagination' => self::getPaginationHtml()
			];
		}

		Helper::ajaxResponse($response); //returns JSON and dies inside
	}

	public static function getMessageListHtml() {
		return Helper::getHtml('message_list.php');
	}

	public static function getPaginationHtml() {
		return Helper::getHtml('pagination.php');
	}
}