<?php
/* DB CONFIG in mysqldatabase.php */

Class Config {
	public static $messagesPerPage = 5;
	public static $assetRelUrl = 'assets/';	
	public static $errorClass = 'err';	
	public static $validClass = 'valid';
	public static $absentClass = 'no-validation-performed-yet';
	public static $dateFormat = 'Y-m-d';	
}