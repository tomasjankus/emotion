<?php

Class MySqlDatabase {
	private static $username = "root";
	private static $password = "none";
	private static $host = "localhost";
	private static $database = "tj_emotion";
	private static $instance = null;

	public function __construct() {
		return null;
	}

	public static function getInstance() {
		if (empty(self::$instance)) {
			self::$instance = new mysqli(self::$host, self::$username, self::$password, self::$database);
		}
		return self::$instance;
	}
}